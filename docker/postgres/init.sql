CREATE USER booking with password 'booking';
CREATE DATABASE booking;
GRANT ALL PRIVILEGES ON DATABASE booking TO booking;


CREATE TABLE FLIGHT (
	id    serial PRIMARY KEY ,
	departure	varchar(30) not null,
	destination	varchar(30) not null,
    maxSeats	integer not null,
	bookedSeats integer not null,
	flightDate date,
	serviceClassBasePrice integer not null,
    serviceClassMaxPrice integer not null,
	availabilityAdjustmentFactor decimal not null,	
	lines integer not null,
	seats integer not null
);

CREATE TABLE BOOKING (
	id    serial PRIMARY KEY,
	id_flight   integer not null references flight(id), 
	name	varchar(30) not null,
	surname	varchar(30) not null,
    sericeLevel	integer not null,
	price integer not null,
	line integer not null,
	seat integer not null,
	PRN varchar(20) not null
);


CREATE INDEX pnr_unique ON BOOKING (id_flight, PNR);


insert into flight ("departure","destination","maxseats","bookedseats","flightdate","serviceclassbaseprice","availabilityadjustmentfactor","serviceclassmaxprice","lines","seats") values ('rome','new york',410,0,'2017-09-11',150,0.5,600,5,60);

insert into flight ("departure","destination","maxseats","bookedseats","flightdate","serviceclassbaseprice","availabilityadjustmentfactor","serviceclassmaxprice","lines","seats") values ('rome','new york',410,0,'2017-11-11',150,0.5,600,5,60);

insert into flight ("departure","destination","maxseats","bookedseats","flightdate","serviceclassbaseprice","availabilityadjustmentfactor","serviceclassmaxprice","lines","seats") values ('rome','new york',410,0,'2017-11-21',150,0.5,600,5,60);


insert into flight ("departure","destination","maxseats","bookedseats","flightdate","serviceclassbaseprice","availabilityadjustmentfactor","serviceclassmaxprice","lines","seats") values ('rome','tokio',380,0,'2017-09-01',150,0.7,1200,4,80);

insert into flight ("departure","destination","maxseats","bookedseats","flightdate","serviceclassbaseprice","availabilityadjustmentfactor","serviceclassmaxprice","lines","seats") values ('rome','tokio',380,0,'2017-10-01',150,0.7,1200,4,80);

insert into flight ("departure","destination","maxseats","bookedseats","flightdate","serviceclassbaseprice","availabilityadjustmentfactor","serviceclassmaxprice","lines","seats") values ('rome','tokio',380,0,'2017-11-01',150,0.7,1200,4,80);

