<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="it.booking.BookingService"%>
<%@ page import="it.booking.*"%>
<%@ page import="java.util.List"%>
<%@ page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="/css/main.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Flight web-app application</title>
</head>
<body>

	<%
		List<Flight> flights = (List<Flight>)request.getAttribute("found");
		FlightService flightsService = new FlightService();
		if (flights==null) 
			flights = flightsService.readAll();
		pageContext.setAttribute("flights", flights);
		int i = 0;
	%>
	<center>

		<table class="table table-bordered">
			<thead>
				<tr>
					<th scope="col">Da</th>
					<th scope="col">A</th>
					<th scope="col">Prezzo Base</th>
					<th scope="col">Prezzo Massimo</th>
					<th scope="col">N� Posti</th>
					<th scope="col">N� Posti Prenotati</th>
					<th scope="col">					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${flights}">
					<tr>
						<td><c:out value="${user.departure}" /></td>
						<td><c:out value="${user.destination}" /></td>
						<td><c:out value="${user.serviceclassbaseprice}" /></td>
						<td><c:out value="${user.serviceclassmaxprice}" /></td>
						<td><c:out value="${user.maxseats}" /></td>
						<td><c:out value="${user.bookedseats}" /></td>
						<td><a href="book.jsp?flight=<c:out value="${user.id}" />">Book</a></td>
						<c:set var="test" value="${user.id}"/>
					</tr>
					<%
						BookingService bookingService = new BookingService();
						i++;
						Long pos = (Long)pageContext.getAttribute("test"); 
						List<Booking> bkns = bookingService.readAllBookings(pos.longValue());
						pageContext.setAttribute("bkns", bkns);
					%>
					<tr>
						<td ></td>
						<td ></td>
						<td colspan="5">
							<table class="table table-bordered">
								<tr>
									<td>Bookings</td>
								</tr>
								<tr>
									<td></td>
									<td>Name</td>
									<td>Surname</td>
									<td>Price</td>
									<td>PNR</td>
								</tr>
								<c:forEach var="book" items="${bkns}">
									<tr>
										<td></td>
										<td><c:out value="${book.name}" /></td>
										<td><c:out value="${book.surname}" /></td>
										<td><c:out value="${book.price}" /></td>
										<td><c:out value="${book.pnr}" /></td>										
									</tr>
								</c:forEach>
							</table>
						</td>
					</tr>

				</c:forEach>
			<tbody>
		</table>
	
</body>
</html>