<?xml version="1.0" encoding="ISO-8859-1" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	version="2.0">
	<jsp:directive.page contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1" session="false" />
	<jsp:output doctype-root-element="html"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="true" />
	<jsp:directive.page import="it.booking.BookingService" language="java" />
	<jsp:directive.page import="it.booking.FlightService" language="java" />
	<jsp:directive.page import="it.booking.Flight" language="java" />
	<jsp:directive.page errorPage="error.jsp" />
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="css/bootstrap.css"></link>
<link rel="stylesheet" href="css/main.css"></link>
<title>Booking Page</title>
</head>
<body>
	<center>
		<jsp:scriptlet>BookingService bookingService = new BookingService();
			Integer[] price = bookingService.calculatePrice(request.getParameter("flight"));
			FlightService flightService = new FlightService();
			Flight flight = flightService.read(Long.parseLong(request.getParameter("flight")));
			pageContext.setAttribute("price", price);</jsp:scriptlet>

		<table class="table table-bordered">
			<thead>
			</thead>
			<tbody>
				<tr>
					<td>
						<form action="book" method="POST" class="container"
							id="needs-validation" novalidate="true">
							<div class="row">
								<div class="col-md-6 mb-3">
									<label for="validationCustom01">First name</label> <input
										required="true" type="text" name="first_name"
										class="form-control" placeholder="Text input" maxlength="40" />
								</div>
								<div class="col-md-6 mb-3">
									<label for="validationCustom02">Last name</label> <input
										required="true" type="text" name="last_name"
										class="form-control" placeholder="Text input" maxlength="40" />
								</div>
							</div>

							<div class="row"><center>
								<select class="custom-select d-block my-3" required="true"
									name="level">
									<option value="">Select Service Level</option>
									<option value="1">Economy ${price[0]}</option>
									<option value="2">Business ${price[1]}</option>
									<option value="3">Executive ${price[2]}</option>
								</select> <input type="hidden" name="idFlight" value="${param.flight}" />
								</center>
							</div>

							<div class="row">
							<center>
								<table>
									<jsp:scriptlet>
										int rows = flight.getLines().intValue();
										int cols = flight.getSeats().intValue();
										pageContext.setAttribute("rows", rows);
										pageContext.setAttribute("cols", cols);
									</jsp:scriptlet>
									<c:forEach var="row" begin="1" end="${rows}">
										<tr>
											<c:forEach var="line" begin="1" end="${cols}">
												<td><input type="radio" name="position"
													value="${row}:${line}" required="true"></input></td>
											</c:forEach>
										</tr>
									</c:forEach>
								</table>
							</center>
							</div>

							<input type="submit" value="Book Flight" />
						</form>
					</td>
				</tr>
			</tbody>
		</table>

	</center>
</body>
	</html>
</jsp:root>