<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="it.booking.*"%>
<%@ page import="java.util.List"%>
<%@ page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="/css/main.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Flight web-app application</title>
</head>
<body>
	<center>

		<table class="table table-bordered">
			<thead>
			</thead>
			<tbody>
				<tr>
					<td>
						<form action="search" method="POST" class="container"
							id="needs-validation" novalidate="true">
							<div class="row">
								<div class="col-md-6 mb-3">
									<label for="validationCustom01">Partenza</label> <input
										required="true" type="text" name="from" class="form-control"
										placeholder="Text input" maxlength="20" />
								</div>
								<div class="col-md-6 mb-3">
									<label for="validationCustom02">Destinazione</label> <input
										required="true" type="text" name="to" class="form-control"
										placeholder="Text input" maxlength="20" />
								</div>
							</div>

							<div class="row">
								<div class="col-md-6 mb-3">
									<label for="validationCustom01">Da</label> <input type="date"
										name="dateFrom" class="form-control"></input>
								</div>
								<div class="col-md-6 mb-3">
									<label for="validationCustom02">A</label> <input type="date"
										name="dateTo" class="form-control"></input>
								</div>
							</div>

							<input type="submit" value="Search Flight" />
						</form>
					</td>
				</tr>
			</tbody>
		</table>

	</center>
</body>
</html>