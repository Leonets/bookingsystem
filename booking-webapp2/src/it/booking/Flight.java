package it.booking;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the flight database table.
 * 
 */
@Entity
@NamedQuery(name="Flight.findAll", query="SELECT f FROM Flight f")
public class Flight implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Flight() {
	}


	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id",unique=true, nullable = false)		
	private Long id;

//  	private BigDecimal availabilityadjustmentfactor;

  	private Integer bookedseats;
  
  	private String departure;
  
  	private String destination;
  
  	@Temporal(TemporalType.DATE)
  	@JsonIgnore
  	private Date flightdate;

  	private Integer maxseats;
  
  	private Integer serviceclassbaseprice;
  
  	private Integer serviceclassmaxprice;

//	  bi-directional many-to-one association to Booking
//	@OneToMany
//	@JsonIgnoreProperties("flight")
//	private List<Booking> bookings;

  	private Integer lines;
  	
  	private Integer seats;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

//	public BigDecimal getAvailabilityadjustmentfactor() {
//		return this.availabilityadjustmentfactor;
//	}
//
//	public void setAvailabilityadjustmentfactor(BigDecimal availabilityadjustmentfactor) {
//		this.availabilityadjustmentfactor = availabilityadjustmentfactor;
//	}

	public Integer getLines() {
		return lines;
	}

	public void setLines(Integer lines) {
		this.lines = lines;
	}

	public Integer getSeats() {
		return seats;
	}

	public void setSeats(Integer seats) {
		this.seats = seats;
	}

	public Integer getBookedseats() {
		return this.bookedseats;
	}

	public void setBookedseats(Integer bookedseats) {
		this.bookedseats = bookedseats;
	}

	public String getDeparture() {
		return this.departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDestination() {
		return this.destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

//	public Date getFlightdate() {
//		return this.flightdate;
//	}

	
	public void setFlightdate(Date flightdate) {
		this.flightdate = flightdate;
	}

	public Integer getMaxseats() {
		return this.maxseats;
	}

	public void setMaxseats(Integer maxseats) {
		this.maxseats = maxseats;
	}

	public Integer getServiceclassbaseprice() {
		return this.serviceclassbaseprice;
	}

	public void setServiceclassbaseprice(Integer serviceclassbaseprice) {
		this.serviceclassbaseprice = serviceclassbaseprice;
	}

	public Integer getServiceclassmaxprice() {
		return this.serviceclassmaxprice;
	}

	public void setServiceclassmaxprice(Integer serviceclassmaxprice) {
		this.serviceclassmaxprice = serviceclassmaxprice;
	}

//	@JsonManagedReference
//	@JsonIgnore
//	public List<Booking> getBookings() {
//		return this.bookings;
//	}
//	@JsonIgnore
//	public void setBookings(List<Booking> bookings) {
//		this.bookings = bookings;
//	}

//	public Booking addBooking(Booking booking) {
//		getBookings().add(booking);
//		booking.setFlight(this);
//		return booking;
//	}
//
//	public Booking removeBooking(Booking booking) {
//		getBookings().remove(booking);
//		booking.setFlight(null);
//		return booking;
//	}

}