package it.booking;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.PathParam;

import it.booking.listener.Listener;

public class BookingService {

	EntityManager em = Listener.createEntityManager();

	// /**
	// *
	// * @param id
	// * @return
	// */
	// public Booking read(@PathParam("id") long id) {
	// EntityManager em = Listener.createEntityManager();
	// try {
	// return em.find(Booking.class, id);
	// } finally {
	// em.close();
	// }
	// }
	//

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Booking book(@PathParam("id") long id) {
		EntityManager em = Listener.createEntityManager();
		try {
			return em.find(Booking.class, id);
		} finally {
			em.close();
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public List<Booking> readAllBookings(@PathParam("id") long id) {
		EntityManager em = Listener.createEntityManager();
		try {

			TypedQuery<Booking> query = em.createQuery("SELECT c FROM Booking c WHERE c.flight = :id", Booking.class);
			Flight fligth = new Flight();
			fligth.setId(new Long(id));
			query.setParameter("id", fligth);

			List<Booking> fs = (List<Booking>) query.getResultList();
			return fs;

		} finally {
			em.close();
		}
	}

	public List<String> query(Integer idFlight) {

		List<Booking> bookings = this.readAllBookings(idFlight);
			
		List<String> booked = new ArrayList<String>();
		for (Booking booking : bookings) {
			String seat = booking.getLine()+":"+booking.getSeat();
			booked.add(seat);
			System.out.println("booked " + seat);
		}	
		
		return booked;
	}

	/**
	 * 
	 * @param first_name
	 * @param last_name
	 * @param economy
	 * @param business
	 * @param executive
	 * @param idFlight
	 * @return
	 */
	public String book(String first_name, String last_name, String level, String idFlight, String line, String seat) {
		EntityManager em = Listener.createEntityManager();
		String PNR = null;

		try {
			em.getTransaction().begin();
			Booking booking = new Booking();
			booking.setName(first_name);
			booking.setSurname(last_name);
			int serviceLevel = Integer.parseInt(level);
			booking.setServicelevel(serviceLevel);

			Flight flight = em.find(Flight.class, new Long(idFlight));

			Integer[] price = calculatePrice(flight.getMaxseats(), flight.getBookedseats(),
					flight.getServiceclassbaseprice(), flight.getServiceclassmaxprice());
			System.out.println("Price/Level " + price[0] + " - " + price[1] + " - " + price[2]);

			flight.setBookedseats(flight.getBookedseats() + 1);
			booking.setFlight(flight);

			booking.setPrice(new Integer(price[serviceLevel - 1].intValue()));

			booking.setLine(Integer.parseInt(line));
			booking.setSeat(Integer.parseInt(seat));
			PNR = calculatePnr();
			System.out.println("PNR " + PNR);
			booking.setPnr(PNR);

			em.persist(booking);
			em.getTransaction().commit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}

		return PNR;
	}

	/**
	 * 
	 * @param PNR
	 * @return
	 */
	public Booking search(String PNR) {
		EntityManager em = Listener.createEntityManager();
		try {

			TypedQuery<Booking> query = em.createQuery("SELECT c FROM Booking c WHERE c.pnr = :pnr", Booking.class);
			query.setParameter("pnr", PNR);

			Booking fs = (Booking) query.getSingleResult();
			return fs;

		} finally {
			em.close();
		}
	}

	/**
	 * 
	 * @param idFlight
	 * @return
	 * @throws Exception
	 */
	public Integer[] calculatePrice(String idFlight) throws Exception {

		Flight flight;

		try {
			flight = em.find(Flight.class, new Long(idFlight));
		} finally {
			em.close();
		}

		return calculatePrice(flight.getMaxseats(), flight.getBookedseats(), flight.getServiceclassbaseprice(),
				flight.getServiceclassmaxprice());

	}

	/**
	 * 
	 * @param maxSeats
	 * @param bookedSeats
	 * @param basePrice
	 * @param maxPrice
	 * @throws Exception
	 */
	private Integer[] calculatePrice(Integer maxSeats, Integer bookedSeats, Integer basePrice, Integer maxPrice)
			throws Exception {

		float div = (bookedSeats) / maxSeats.floatValue();
		BigDecimal loadFactor = BigDecimal.valueOf(div);
		loadFactor.setScale(2, BigDecimal.ROUND_HALF_UP);
		MathContext mc = new MathContext(2, RoundingMode.HALF_UP);
		System.out.println("LOAD FACTOR " + loadFactor.round(mc));

		Integer adjusted = (int) (basePrice * (1 + loadFactor.round(mc).doubleValue()));
		System.out.println("ADJUSTED " + adjusted);

		Integer priceBusiness = (int) (adjusted / 100 * 15 + adjusted);
		Integer priceExecutive = (int) (adjusted / 100 * 25 + adjusted);

		List<Integer> prices = Arrays.asList(new Integer[] { adjusted, maxPrice });
		IntSummaryStatistics flightPrice = prices.stream().mapToInt((x) -> x).summaryStatistics();
		System.out.println("Highest price eco : " + flightPrice.getMax());
		System.out.println("Lowest price eco : " + flightPrice.getMin());

		List<Integer> pricesBus = Arrays.asList(new Integer[] { priceBusiness, maxPrice });
		IntSummaryStatistics flightPriceBus = pricesBus.stream().mapToInt((x) -> x).summaryStatistics();
		System.out.println("Highest price bus : " + flightPriceBus.getMax());
		System.out.println("Lowest price bus : " + flightPriceBus.getMin());

		List<Integer> pricesEx = Arrays.asList(new Integer[] { priceExecutive, maxPrice });
		IntSummaryStatistics flightPriceEx = pricesEx.stream().mapToInt((x) -> x).summaryStatistics();
		System.out.println("Highest price exe  : " + flightPriceEx.getMax());
		System.out.println("Lowest price exe : " + flightPriceEx.getMin());

		return new Integer[] { flightPrice.getMin(), flightPriceBus.getMin(), flightPriceEx.getMin() };
	}

	private String calculatePnr() {
		String PNR = "";
		int[] numbers = new Random().ints(0, 990).limit(6).toArray();
		PNR = "A" + numbers[0] + numbers[1] + numbers[2] + "X" + numbers[3] + numbers[4] + numbers[5];
		return PNR;
	}

}
