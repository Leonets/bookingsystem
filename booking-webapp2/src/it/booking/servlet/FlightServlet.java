package it.booking.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.booking.Flight;
import it.booking.FlightService;

/**
 * Servlet implementation class BookingServlet
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/FlightServlet" })
public class FlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	private FlightService flightService = new FlightService();

	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FlightServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		SimpleDateFormat in = new SimpleDateFormat("yyyy-MM-dd");
		String dateFrom = request.getParameter("dateFrom");
		String dateTo = request.getParameter("dateTo");
		Date datesFrom = null;
		Date datesTo = null;
		try {
			datesFrom = in.parse(dateFrom);
			datesTo = in.parse(dateTo);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("from" + dateFrom);
		System.out.println("to" + dateTo);
		 
		List<Flight> found = flightService.search(from, to, datesFrom, datesTo);
		
		request.setAttribute("found", found);
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);   
	}

}
