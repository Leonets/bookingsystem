package it.booking.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.booking.BookingService;

/**
 * Servlet implementation class BookingServlet
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/BookingServlet" })
public class BookingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private BookingService bookingService = new BookingService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookingServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String idFlight = request.getParameter("idFlight");
		String level = request.getParameter("level");

		String position = request.getParameter("position");
		checkValues(firstName, lastName, idFlight, level, position, request, response);
		
		String[] pos = position.split(":");
		System.out.println("position" + position);

		bookingService.book(firstName, lastName, level, idFlight, pos[0], pos[1]);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
		dispatcher.forward(request, response);
	}

	private void checkValues(String firstName, String lastName, String idFlight, String level, String position,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (firstName == null || lastName == null || level == null || position == null) {
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/book.jsp");
			request.setAttribute("mandatory", "true");
			request.setAttribute("flight", idFlight);
			dispatcher.forward(request, response);
		}
	}

}
