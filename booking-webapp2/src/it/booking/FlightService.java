package it.booking;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.PathParam;

import it.booking.listener.Listener;

public class FlightService {

	public List<Flight> readAll() {
		long start = System.currentTimeMillis();
		System.out.println("FlighService.readAll() started");
		EntityManager em = Listener.createEntityManager();
		try {

			TypedQuery<Flight> q = em.createQuery("from Flight e", Flight.class);
			List<Flight> fs = (List<Flight>) q.getResultList();
			System.out.println("EmployeeController.readAll() started" + fs.size());
			return fs;

		} finally {
			em.close();
			System.out.println("Getting data took " + (System.currentTimeMillis() - start) + "ms.");
		}
	}

	public Flight read(@PathParam("id") long id) {
		long start = System.currentTimeMillis();
		System.out.println("FlighService.read() started");
		EntityManager em = Listener.createEntityManager();
		try {
			Flight flight =  em.find(Flight.class, id);
			return flight;

		} finally {
			em.close();
			System.out.println("Getting data took " + (System.currentTimeMillis() - start) + "ms.");
		}
	}
	
	
	public List<Flight> search(@PathParam("from") String from, @PathParam("to") String to, 
			@PathParam("dateFrom") Date dateFrom, @PathParam("dateTo") Date dateTo) {
		long start = System.currentTimeMillis();
		System.out.println("FlighService.search() started");
		EntityManager em = Listener.createEntityManager();
		try {			
			TypedQuery<Flight> query = em.createQuery(
					"SELECT c FROM Flight c " + "WHERE c.departure like :from||'%' " 
								+ " and c.destination like :to||'%' "
								+ " and c.flightdate >= :dateFrom "
								+ " and c.flightdate <= :dateTo ",
					Flight.class);
			query.setParameter("from", from);
			query.setParameter("to", to);
			query.setParameter("dateFrom", dateFrom);
			query.setParameter("dateTo", dateTo);

			List<Flight> fs = (List<Flight>) query.getResultList();
			return fs;

		} finally {
			em.close();
			System.out.println("Getting data took " + (System.currentTimeMillis() - start) + "ms.");
		}
	}
	
	
	


}
