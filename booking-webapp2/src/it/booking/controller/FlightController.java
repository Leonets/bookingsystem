package it.booking.controller;

import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import it.booking.Booking;
import it.booking.Flight;
import it.booking.FlightService;

@Path("/flight")
public class FlightController {

	
	private FlightService flightService = new FlightService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Flight read(@PathParam("id") long id) {
		Flight flight = flightService.read(id);
		return flight;
	}

	
	public List<Flight> search(@PathParam("from") String from, @PathParam("to") String to, 
			@PathParam("dateFrom") Date dateFrom, @PathParam("dateTo") Date dateTo) {
		
		return flightService.search(from, to, dateFrom, dateTo);
	}
}
