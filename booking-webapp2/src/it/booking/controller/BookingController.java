package it.booking.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import it.booking.Booking;
import it.booking.BookingService;

@Path("/booking")
public class BookingController {

	
	private BookingService bookService = new BookingService();

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("search/{PNR}")
	public Booking searchPNR(@PathParam("PNR") String PNR) {
		Booking book = bookService.search(PNR);
		return book;
	}

	public BookingController() {
	}

	
	@POST
	@Path("book")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String verify(@FormParam("first_name") String first_name,
						@FormParam("last_name") String last_name,
						@FormParam("level") String level,
						@FormParam("idFlight") String idFlight,
						@FormParam("line") String line,
						@FormParam("seat") String seat,
	@Context HttpServletResponse servletResponse) throws IOException {
	    String code = bookService.book(first_name,last_name,level,idFlight,line,seat);
	    return code;
	}
		

}
